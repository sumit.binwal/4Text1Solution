//
//  CommonFunctions.m
//  kiplApps
//
//  Created by Konstant on 22/05/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//
#import <CoreText/CoreText.h>
#import "CommonFunctions.h"
#import "AppDelegate.h"

@implementation CommonFunctions


+ (NSString *)documentsDirectory {
    NSArray *paths = 
	NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, 
										NSUserDomainMask, 
										YES);
    return [paths objectAtIndex:0];
    
    //comment from Amzad
}

+ (void)openEmail:(NSString *)address {
    NSString *url = [NSString stringWithFormat:@"mailto://%@", address];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

+ (void)openPhone:(NSString *)number {
    NSString *url = [NSString stringWithFormat:@"tel://%@", number];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

+ (void)openSms:(NSString *)number {
    NSString *url = [NSString stringWithFormat:@"sms://%@", number];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

+ (void)openBrowser:(NSString *)url {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

+ (void)openMap:(NSString *)address {
	NSString *addressText = [address stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];	
	NSString *url = [NSString stringWithFormat:@"http://maps.google.com/maps?q=%@", addressText];
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}


 
+(void) checkAndCreateDatabase{
	// Check if the SQL database has already been saved to the users phone, if not then copy it over
	BOOL success;
	
	// Create a FileManager object, we will use this to check the status
	// of the database and to copy it over if required
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	// Check if the database has already been created in the users filesystem
	success = [fileManager fileExistsAtPath:DatabasePath];
	
	// If the database already exists then return without doing anything
	if(success) return;
	
	// If not then proceed to copy the database from the application to the users filesystem
	
	// Get the path to the database in the application package
	NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DatabaseName];
	
	// Copy the database from the package to the users filesystem
	[fileManager copyItemAtPath:databasePathFromApp toPath:DatabaseName error:nil];
	
 
}

+ (NSString*) getNibNameForName:(NSString*) name
{
    NSString *nibName = [NSString stringWithFormat:IS_IPAD?@"%@_iPad":@"%@",name];
    
    return nibName;
}

+ (NSString*) getImageNameForName:(NSString*) name
{
    NSString *imgName = [NSString stringWithFormat:IS_IPAD?@"%@_iPad":@"%@",name];
    return imgName;
}

+ (BOOL)isRetineDisplay{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
        ([UIScreen mainScreen].scale == 2.0)) {
        // Retina display
        return YES;
    } else {
        // not Retine display
        return NO;
    }
}

+(BOOL)isiPad
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        isIpad=YES;
    }
    else {
        isIpad=NO;
    }
    return isIpad;
}

+(void) iphone5Check
{
    CGRect screenRect = [[UIScreen mainScreen] applicationFrame];
    
    if  (!isIpad)
    {
        if (screenRect.size.height >= 548  && screenRect.size.height < 600)
        {
            isiPhone5 = YES;
        }else
        {
            isiPhone5 = NO;
        }
    }
}

+(void)setLocalNotification
{
    localNotification = nil;
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    NSDate *date = [NSDate date];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *components = [calendar components: NSUIntegerMax fromDate: date];
    
    NSArray *tempTimeArray = [[DEFAULTS objectForKey:@"remindertime"] componentsSeparatedByString:@":"];
    
    int hours = [[tempTimeArray objectAtIndex:0] intValue];
    
    if ([[tempTimeArray objectAtIndex:1] rangeOfString:@"AM"].location == NSNotFound)
        hours = hours + 12;
    
    [components setHour:hours];
    [components setMinute:[[tempTimeArray objectAtIndex:1] intValue]];
    [components setSecond:00];
    
    NSDate *currentdateTime = [calendar dateFromComponents:components];
    
    NSDate *pickerDate = currentdateTime;
    
    localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = pickerDate;
    localNotification.alertBody = localNotificationMsg;
    localNotification.alertAction = localNotificationMsg;
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.repeatInterval=NSDayCalendarUnit;
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

+(UIImage*)captureScreen:(UIView*)view
{
	UIGraphicsBeginImageContext(view.frame.size);
	[view.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
    
    return viewImage;
}
@end
