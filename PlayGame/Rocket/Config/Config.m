//
//  Config.M
//  WhatzzApp
//
//  Created by Konstant on 22/05/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import "Config.h"


@implementation Config

/**** set intial coins ****/
int   coins         =   1000;
int   winCoins      =    4;
int   deleteCoins   =   80;
int   showCoins     =   60;
int   skipCoins     =   100;
int   bonusCoins    =   20;
int   rateAppCoins  =    3;
int   shareOnFbCoins =   3;
int   watchVideoAdsCoins =   3;

/**** variables for current device  ****/
BOOL isIpad;
BOOL isiPhone5;

NSString	*SiteAPIURL				= @"";
NSString    *rateURL                = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=";

NSMutableArray *puzzleArray;
NSMutableDictionary *puzzleDictionary;

/**** Database name ****/
NSString	*DatabaseName			= @"PlayGame.sqlite";
NSString	*DatabasePath;

/**** All key ****/
NSString    *flurryKey              = @"H3TCTB5BVDW63ZH65J54";
NSString    *AppraterId             = @"798526650";

NSString    *AdColonyAppID          = @"app670c94e41ef24579b70083";
NSString    *AdColonyZoneID         = @"vza4a3ce68e86b4b61bd585c";

NSString    *ChartboostAppID        = @"52d7960f2d42da59cd8091af";
NSString    *chartboostSignatureKey = @"bd018fe05ad4cf4807e398659ccd89b000ed832b";
NSString	*revMobKey              = @"52627d7400893d9d5e000017";
NSString    *iTunesAppID        = @"728414735";


NSString	*currentPuzzleNumber    = @"1";

NSString	*moreAppURL				= @"http://support24hour.com/workplace2/xmlApp/sample.xml";
NSString	*moreAppButtonUrl       = @"http://www.kiplapps.com";

/******** variables for messages ************/
NSString    *subjectMsg           = @"4 Words 1 Solution";
NSString    *mailAddress            = @"feedback@kiplapps.com";

NSString	*FBHomeMsg            = @"Voila! I just love it. Try it: www.kiplapps.com/r/30p @KIPLApps";
NSString	*FBSpecialDayMsg  = @"Ohhh, Great to find you here! Try it: www.kiplapps.com/r/30p @KIPLApps";
 NSString	*FBhelpMsg = @"Can you help me? What word is described by these puzzle?";

NSString	*twitterMsg             = @"Wow, I’m really enjoying this amazing free app. Try it: www.kiplapps.com/r/30p @KIPLApps";
NSString	*twitterSpecialDayMsg   = @"Wow, I’m really enjoying this amazing free app. Try it: www.kiplapps.com/r/30p @KIPLApps";

NSString    *rateThisAppMsg         = @"If you enjoy this app, would you mind taking a moment to rate it?";


NSString    *Congrats               = @"Awesome!";
NSString    *rightString            = @"You've guessed the word.";

/******** variables for LocalNotification ************/
NSString	*localNotificationMsg   =   @"Hey, You are Ready to play.";
UILocalNotification *localNotification;


/******** variables for IAP ************/
NSArray     *productArr;
NSString	*removeAddProductId=@"4words1solution.remove.ads";

@end