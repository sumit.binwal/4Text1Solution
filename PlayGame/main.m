//
//  main.m
//  PlayGame
//
//  Created by Chandan Kumar on 04/03/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
