//
//  SettingsViewController.m
//  FourPicsOneWord
//
//  Created by  on 10/16/13.
//  Copyright (c) 2013 Vojtec. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (BOOL)prefersStatusBarHidden {
    return YES;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
   self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:[CommonFunctions getImageNameForName:@"background"]]];
    
    _topConstraint.constant = isiPhone5?20:10;
    _firstMidConstraint.constant =isiPhone5?26:0;
    _midConstraint.constant =isiPhone5?38:10;
    _bottomConstraint.constant =isiPhone5?24:10;
    
    //sounds
    [(UITextField*)[self.view viewWithTag:10000] setFont:[UIFont fontWithName:@"RobotoSlab-Regular" size:IS_IPAD?22:12]];
     [(UITextField*)[self.view viewWithTag:10000] setText:[NSString stringWithFormat:@"   %@", NSLocalizedString(@"Sounds", nil)]];
    //free hints notif
    [(UITextField*)[self.view viewWithTag:20000] setFont:[UIFont fontWithName:@"RobotoSlab-Regular" size:IS_IPAD?22:12]];
    [(UITextField*)[self.view viewWithTag:20000] setText:[NSString stringWithFormat:@"   %@", NSLocalizedString(@"hintsPush", nil)]];
    
    
    [_soundSwitch setThumbTintColor:[UIColor whiteColor]];
    [_soundSwitch setOnTintColor:UIColorFromRedGreenBlue(3, 34, 117)];
    _soundSwitch.on = ((NSNumber*)[DEFAULTS objectForKey:@"sounds"]).boolValue;
    
    [_notificationSwitch setThumbTintColor:[UIColor whiteColor]];
    [_notificationSwitch setOnTintColor:UIColorFromRedGreenBlue(3, 34, 117)];
    _notificationSwitch.on = ((NSNumber*)[DEFAULTS objectForKey:@"freeHintsNotif"]).boolValue;
    
    [[(UIButton*)[self.view viewWithTag:30000] titleLabel] setFont:[UIFont fontWithName:@"RobotoSlab-Bold" size:IS_IPAD?27:15]];
    [[(UIButton*)[self.view viewWithTag:40000] titleLabel] setFont:[UIFont fontWithName:@"RobotoSlab-Bold" size:IS_IPAD?27:15]];
    [[(UIButton*)[self.view viewWithTag:50000] titleLabel] setFont:[UIFont fontWithName:@"RobotoSlab-Bold" size:IS_IPAD?27:15]];
    
    [(UIButton*)[self.view viewWithTag:60000] setTitle:mailAddress forState:UIControlStateNormal];
    [(UIButton*)[self.view viewWithTag:70000] setTitle:[NSString stringWithFormat:@"                     %@",[moreAppButtonUrl stringByReplacingOccurrencesOfString:@"http://" withString:@""]] forState:UIControlStateNormal];
    
    //*******create Database object ********//
    dbRecord =[[DatabaseHandler alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-
#pragma mark IBActions
-(IBAction)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)toggleHints:(UISwitch*)sender
{
   [DEFAULTS setObject:@(sender.on) forKey:@"freeHintsNotif"];
   [DEFAULTS synchronize];
}

-(IBAction)toggleSounds:(UISwitch*)sender
{
   [DEFAULTS setObject:@(sender.on) forKey:@"sounds"];
   [DEFAULTS synchronize];
}
- (IBAction)openMoreAppUrl:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",moreAppButtonUrl]]];
}

- (IBAction)openMail:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:subjectMsg];
        NSArray *toRecipients = [NSArray arrayWithObjects:mailAddress, nil];
        [mailer setToRecipients:toRecipients];
        
        [self presentViewController:mailer animated:TRUE completion:nil];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Oops!"
                                    message:@"Please configure atleast one account for sending mail." delegate:nil
                          cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}

# pragma mark - MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    // Remove the mail view
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

#pragma mark-
#pragma mark UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 10001)
    {
        if (buttonIndex == 1) {
            currentPuzzleNumber = @"1";
            [DEFAULTS setBool:NO forKey:@"isFirst"];
            [DEFAULTS setObject:currentPuzzleNumber forKey:@"currentPuzzleNo"];
            [DEFAULTS synchronize];
            
            [dbRecord executeQuery:@"Delete FROM PuzzleTable"];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

#pragma mark-
#pragma mark IBActions

- (IBAction)restartButtonPressed:(id)sender
{
   UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Do you want to restart?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    alert.tag =10001;
    [alert show];
}

- (IBAction)reviewAppButtonPressed:(id)sender {
}

- (IBAction)restorePurchaseButtonPressed:(id)sender {
}
@end
