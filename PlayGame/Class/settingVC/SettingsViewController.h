//
//  SettingsViewController.h
//  FourPicsOneWord
//
//  Created by  on 10/16/13.
//  Copyright (c) 2013 Vojtec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface SettingsViewController : UIViewController<MFMailComposeViewControllerDelegate>
{
    DatabaseHandler *dbRecord;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstMidConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *midConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;

@property (weak, nonatomic) IBOutlet UISwitch *soundSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *notificationSwitch;

- (IBAction)restartButtonPressed:(id)sender;
- (IBAction)reviewAppButtonPressed:(id)sender;
- (IBAction)restorePurchaseButtonPressed:(id)sender;


@end
