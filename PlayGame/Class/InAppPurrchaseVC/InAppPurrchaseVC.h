//
//  InAppPurrchaseVC.h
//  PlayGame
//
//  Created by Chandan Kumar on 09/04/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <StoreKit/StoreKit.h>
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import <AdColony/AdColony.h>

#import "Chartboost.h"
#import "Appirater.h"

@interface InAppPurrchaseVC : UIViewController<UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate,AdColonyDelegate, ChartboostDelegate>
{
    NSArray *extraProductIds;

    NSMutableArray *sortProducts;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *crossConstraint;

@property (weak, nonatomic) IBOutlet UITableView *iapTableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *iapActivityIndicator;

@end
