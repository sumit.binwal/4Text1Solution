//
//  MoreAppVC.h
//  PlayGame
//
//  Created by Chandan Kumar on 10/04/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreAppVC : UIViewController<UITabBarDelegate,UITableViewDataSource>
{
    NSMutableArray *moreArray;
}

@property(nonatomic,strong)NSMutableArray *moreArray;

@end
