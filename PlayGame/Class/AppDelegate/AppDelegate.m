//
//  AppDelegate.m
//  PlayGame
//
//  Created by Chandan Kumar on 04/03/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt. Ltd. All rights reserved.
//

#import "AppDelegate.h"

#import "HomeViewController.h"
#import "Chartboost.h"
#import <RevMobAds/RevMobAds.h>
#import <AdColony/AdColony.h>
#import "RageIAPHelper.h"

@interface AppDelegate () <UIAlertViewDelegate, AdColonyDelegate, ChartboostDelegate>
{
}
@end

@implementation AppDelegate

@synthesize _priceFormatter;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [DatabaseHandler checkAndCreateDatabase];
    
    productArr = [[NSArray alloc]init];
    
    [RageIAPHelper sharedInstance];
    
    [[RageIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (success) {
            productArr = products;
             NSLog(@" -------- productArr %@ ", productArr);
        }
    }];

    _priceFormatter = [[NSNumberFormatter alloc] init];
    [_priceFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [_priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    [CommonFunctions isiPad];
    
    [CommonFunctions iphone5Check];

    if (![DEFAULTS objectForKey:@"freeHintsNotif"]) {
        [DEFAULTS setObject:@(YES) forKey:@"freeHintsNotif"];
        [DEFAULTS setObject:@(YES) forKey:removeAddProductId];
        [DEFAULTS setObject:@(YES) forKey:@"sounds"];
        [DEFAULTS setObject:@(NO) forKey:@"rate"];
        [DEFAULTS synchronize];
    }
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    [RevMobAds startSessionWithAppID:revMobKey];
    
    if (((NSNumber*)[DEFAULTS objectForKey:removeAddProductId]).boolValue)
        [[RevMobAds session] showFullscreen];
    
    /***** For video ads. *****/
    [AdColony configureWithAppID:AdColonyAppID zoneIDs:@[AdColonyZoneID] delegate:self logging:YES];
    
    [UIApplication sharedApplication].statusBarHidden = YES;
    
    self.viewController = [[HomeViewController alloc] initWithNibName:[CommonFunctions getNibNameForName:@"HomeViewController"] bundle:[NSBundle mainBundle]];
    
    self.navController = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    self.navController.navigationBarHidden=YES;
//    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = self.navController;
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    UILocalNotification *notification;
    
    if (((NSNumber*)[DEFAULTS objectForKey:@"freeHintsNotif"]).boolValue) {
        notification = [[UILocalNotification alloc] init];
        notification.fireDate = [NSDate dateWithTimeIntervalSinceNow:60*60*24*1];
        currentPuzzleNumber = [DEFAULTS objectForKey:@"currentPuzzleNo"];
        
        //************Create Blank button as per Puzzle Length*****************//
        if (puzzleArray.count) {
            NSString *word = [[puzzleArray objectAtIndex:[currentPuzzleNumber intValue]-1] valueForKey:@"PuzzleName"];
            word = [word stringByReplacingOccurrencesOfString:@" " withString:@""];
          
            NSMutableString *msg = [NSMutableString stringWithString:@"The first letter of the word is: "];
            [msg appendString:((NSString*)[NSString stringWithFormat:@"%c", [word characterAtIndex:0]]).uppercaseString];
            for (int i = 1; i < word.length; i++) {
                [msg appendString:@" _"];
            }
            [msg appendString:@" :)"];
            notification.alertBody = msg;
            notification.soundName = UILocalNotificationDefaultSoundName;
            [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        }
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    // Set icon badge number to zero
    application.applicationIconBadgeNumber = 0;
    
    if (((NSNumber*)[DEFAULTS objectForKey:removeAddProductId]).boolValue)
        [[RevMobAds session] showFullscreen];
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    Chartboost *cb = [Chartboost sharedChartboost];
    cb.appId = ChartboostAppID;
    cb.appSignature = chartboostSignatureKey;
    cb.delegate = self;
    
    [cb startSession];
//   [cb cacheMoreApps];
//   [cb showMoreApps];
//   [cb showInterstitial];
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

+(AppDelegate*) getSharedInstance
{
    AppDelegate *appDelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    return appDelegate;
}
-(void)onAdColonyV4VCReward:(BOOL)success currencyName:(NSString*)currencyName currencyAmount:(int)amount inZone:(NSString*)zoneID
{
    if (success) {
        [self winWatchVideoReward:watchVideoAdsCoins];
    }
}

-(void)winWatchVideoReward:(int)amount
{
    coins += amount;
    [DEFAULTS setObject:[NSNumber numberWithInt:coins] forKey:@"numCoins"];
    [DEFAULTS synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedCoinsAndAds" object:nil];
}

-(void) showHUDWithMessage:(NSString*) message
{
    enableHUD = YES;
    HUD = [[MBProgressHUD alloc] initWithView:self.window];
    HUD.delegate = nil;
    [self.window addSubview:HUD];
    HUD.labelText = @"Please wait";
	HUD.detailsLabelText = message;
    HUD.mode = MBProgressHUDModeIndeterminate;
    HUD.mode = MBProgressHUDModeDeterminate;
    [HUD showWhileExecuting:@selector(myProgressTask) onTarget:self withObject:nil animated:YES];
}
- (void)myProgressTask {
    float progress = 0.0f;
    while (progress < 1.0f) {
        if (!enableHUD) {
            return;
        }
        progress += 0.01f;
        HUD.progress = progress;
        usleep(50000);
        if (progress >= 1.0f) {
            progress = 0.0f;
        }
    }
}
-(void) hideHUD
{
    enableHUD = NO;
}
@end
