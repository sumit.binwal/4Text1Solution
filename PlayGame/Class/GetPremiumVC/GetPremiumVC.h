//
//  GetPremiumVC.h
//  FourPicsOneWord
//
//  Created by  on 10/16/13.
//  Copyright (c) 2013 Vojtec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GetPremiumVC : UIViewController
{
}

@property (weak, nonatomic) UIImage *backgroundImage;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *crossConstraint;

@end
