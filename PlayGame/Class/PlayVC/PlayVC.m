//
//  PlayVC.m
//  PlayGame
//
//  Created by Chandan Kumar on 07/03/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt. Ltd. All rights reserved.
//

#import "PlayVC.h"
#import "GetPremiumVC.h"
#include "InAppPurrchaseVC.h"

@interface PlayVC ()
{
    BOOL bonus;
    BOOL blinkAnimate;
    
    SystemSoundID letterSound;
    SystemSoundID successSound;
    
    BOOL _premium;
}

@end

@implementation PlayVC

@synthesize currentPuzzleName,appendedPuzzleString;

-(void)initAsBonus:(BOOL)bonus_;
{
    if (self) {
        // Custom initialization
        bonus = bonus_;
        numAnsweredInRow = 0;
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        bonus = NO;
        numAnsweredInRow= 0;
    }
    return self;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    coins = ((NSNumber*)[DEFAULTS objectForKey:@"numCoins"]).intValue;
    [self updateCoins];
    
    _premium = ((NSNumber*)[DEFAULTS objectForKey:removeAddProductId]).boolValue;
    
    [self performSelectorOnMainThread:@selector(startTimer) withObject:nil waitUntilDone:NO];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self animateLetters];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout=UIRectEdgeNone;
    
    [self.navigationController setNavigationBarHidden:YES];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:[CommonFunctions getImageNameForName:@"background"]]];
    
    
    /******* for top right button *********/
    _coinButton.titleLabel.font = [UIFont fontWithName:@"RobotoSlab-Bold" size:IS_IPAD?22:15];
    
    //*******Select Data into Database ********//
    dbRecord =[[DatabaseHandler alloc] init];
    query = @"SELECT * FROM PuzzleTable";
    
    if (puzzleArray.count == 0)
        [puzzleArray removeAllObjects];
        
    puzzleArray= nil;
    puzzleArray = [[NSMutableArray alloc] initWithArray:[dbRecord getPuzzleData:query]];
    
    NSLog(@"puzzleArray =====> %@",puzzleArray);
    
    currentPuzzleName=[[NSMutableString alloc] init];
    appendedPuzzleString=[[NSMutableString alloc] init];
    
    /***************** Create system sound object ******************/
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"letters" ofType:@"caf"]];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)(url), &letterSound);
    
    url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"success" ofType:@"m4a"]];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)(url), &successSound);
    /***************** end system sound object ******************/

    
    /******** setUp for letters buttons ***********/
    [self changeView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateCoins) name:@"updatedCoinsAndAds" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteObserver) name:@"deleteObserver" object:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    [zikzakTimer invalidate];
    zikzakTimer = nil;
}
- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [zikzakTimer invalidate];
    zikzakTimer = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-
#pragma mark IBActions
-(IBAction)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)revertAlphabetButtonTouchUpInside:(UIButton*)sender
{
    if (![[(UIButton *)sender currentTitle] isEqualToString:@""])
    {
        blinkAnimate = NO;
        
        if (((NSNumber*)[DEFAULTS objectForKey:@"sounds"]).boolValue) {
            AudioServicesPlaySystemSound(letterSound);
        }
        
        for (int i=0; i<[self.appendedPuzzleArray count]; i++)
        {
            if (100+i==sender.tag)
            {
                
                NSNumber *removeKey;
                for (NSNumber *key in available_chosenMapping) {
                    if (((NSNumber*)available_chosenMapping[key]).intValue ==   i) {
                        removeKey = key;
                        break;
                    }
                }
                
                for (NSNumber* num in revealedLetters) {
                    if (num.intValue == i) {
                        removeKey = nil;
                    }
                }
                
//                NSLog(@"Before remove char appendString==>%@",appendedPuzzleString);
                if (removeKey) {
                    availabilityOfLetters[removeKey.intValue] = @(YES);
                    [available_chosenMapping removeObjectForKey:removeKey];
                    
                    [self.appendedPuzzleArray setObject:@"" atIndexedSubscript:sender.tag-100];
                    appendedPuzzleString=[NSMutableString stringWithFormat:@"%@",[self.appendedPuzzleArray componentsJoinedByString: @""]];
                    
                    
                    //**************Animation Start ******************//
                    
                    CABasicAnimation *fullRotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
                    fullRotation.fromValue = [NSNumber numberWithFloat:0];
                    fullRotation.toValue = [NSNumber numberWithFloat:((-180*M_PI)/180)];
                    fullRotation.duration = .4;
                    fullRotation.repeatCount = 1;
                    [sender.layer addAnimation:fullRotation forKey:@"90"];
                    
                    [UIView animateWithDuration:.4 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^ {
                        sender.alpha=1.0f;
                    }completion:nil];
                    
                    //***************Animation End *******************//
                }
//                NSLog(@"After remove char in appendString==>%@",appendedPuzzleString);
                
                [self updateLetters];
            }
        }
    }
}

-(IBAction)alphabetButtonTouchUpInside:(UIButton*)sender
{
    if ( (appendedPuzzleString.length < currentPuzzleName.length) && (![[(UIButton *)sender currentTitle] isEqualToString:@""]))
    {
        blinkAnimate = NO;
        
        NSLog(@"Before appendedPuzzleString==>%@",appendedPuzzleString);
        for (int i=0; i<self.appendedPuzzleArray.count; i++) {
            if ([[self.appendedPuzzleArray objectAtIndex:i] isEqualToString:@""])
            {
                available_chosenMapping[[NSNumber numberWithInt:sender.tag-1000]] = [NSNumber numberWithInt:i];
                self.appendedPuzzleArray[i] = availablePuzzleArray[sender.tag-1000];
                availabilityOfLetters[sender.tag-1000] = @(NO);

                break;
            }
        }
        appendedPuzzleString=[NSMutableString stringWithFormat:@"%@",[self.appendedPuzzleArray componentsJoinedByString: @""]];
        NSLog(@"After appendedPuzzleString==>%@",appendedPuzzleString);
        
        [self updateLetters];
        
        if (((NSNumber*)[DEFAULTS objectForKey:@"sounds"]).boolValue) {
            AudioServicesPlaySystemSound(letterSound);
        }
        
//        if (appendedPuzzleString.length == currentPuzzleName.length)
//            [self checkIsFillCorrect];
        
    }
    [self checkIsFillCorrect];
}

-(IBAction)faceBookButtonTouchUpInside:(UIButton*)sender
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
        UIGraphicsBeginImageContextWithOptions([AppDelegate getSharedInstance].window.bounds.size, NO, [UIScreen mainScreen].scale);
    else
        UIGraphicsBeginImageContext([AppDelegate getSharedInstance].window.bounds.size);
    [[AppDelegate getSharedInstance].window.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    NSData * data = UIImagePNGRepresentation(image);
    [data writeToFile:@"foo.png" atomically:YES];
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *vc = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [vc addImage:image];
        [vc setInitialText:NSLocalizedString(@"FBhelp", nil)];
        [vc setInitialText:FBhelpMsg];
        
        [vc setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    break;
                    
                default:
                    break;
            }
        }];
        
        [self presentViewController:vc animated:YES completion:nil];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"FaceBook Post" message:@"Please login first in FaceBook!" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
}

-(IBAction)deleteLettersTouchUpInside:(UIButton*)sender
{
    if (coins >= deleteCoins) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:NSLocalizedString(@"DeleteInfo", nil), deleteCoins] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        alert.tag = 10001;
        [alert show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Not enough coins", nil) message:[NSString stringWithFormat:NSLocalizedString(@"Not enough coins details", nil), @(deleteCoins)] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        alert.tag = 10004;
        [alert show];
    }
}

-(IBAction)showCorrectLettersTouchUpInside:(UIButton*)sender
{
    if (coins >= showCoins) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:NSLocalizedString(@"ShowInfo", nil), showCoins] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        alert.tag = 10002;
        [alert show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Not enough coins", nil) message:[NSString stringWithFormat:NSLocalizedString(@"Not enough coins details", nil), @(showCoins)] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        alert.tag = 10004;
        [alert show];
    }
}

-(IBAction)skipPuzzleTouchUpInside:(UIButton*)sender
{
    if (coins >= skipCoins) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:NSLocalizedString(@"SkipInfo", nil), skipCoins] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        alert.tag = 10003;
        [alert show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Not enough coins", nil) message:[NSString stringWithFormat:NSLocalizedString(@"Not enough coins details", nil), @(skipCoins)] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        alert.tag = 10004;
        [alert show];
    }
}

-(IBAction)getCoins
{
    if ([SKPaymentQueue canMakePayments]) {
        InAppPurrchaseVC *iapvc = [[InAppPurrchaseVC alloc] initWithNibName:[CommonFunctions getNibNameForName:@"InAppPurrchaseVC"] bundle:nil];
        iapvc.view.backgroundColor = [UIColor clearColor];
        self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
        
        CATransition *slide = [CATransition animation];
        slide.type = kCATransitionPush;
        slide.subtype = kCATransitionFromTop;
        slide.duration = 0.4;
        slide.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        slide.removedOnCompletion = YES;
        [iapvc.view.layer addAnimation:slide forKey:@"slidein"];
        
        [self presentViewController:iapvc animated:FALSE completion:nil];
        
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"In-App Purchases unavailable" message:@"Please enable In-App purchases in the settings." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark-
#pragma mark Other Methods

/***** Create and set all buttons and bottom view *****/
-(void)changeView
{
    currentPuzzleNumber = [DEFAULTS objectForKey:@"currentPuzzleNo"];
    
    [_numAnsweredLabel setFont:[UIFont fontWithName:@"RobotoSlab-Bold" size:IS_IPAD?28:16]];
    _numAnsweredLabel.text = [NSString stringWithFormat:NSLocalizedString(@"StartPlay", nil), [currentPuzzleNumber intValue]];
    
    self.appendedPuzzleArray=nil;
        
    //************Create Blank button as per Puzzle Length*****************//
    if (puzzleArray.count)
        currentPuzzleName = [[puzzleArray objectAtIndex:[currentPuzzleNumber intValue]-1] valueForKey:@"PuzzleName"];
    NSLog(@"Puzzle Name ==>%@",currentPuzzleName);
    
    _midConstraint.constant=17;//For Iphone only
    
    bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, isIpad?600:isiPhone5?350:330, isIpad?768:320, (isIpad?115:65)*(2+currentPuzzleName.length/6+1))];
    bottomView.backgroundColor=[UIColor clearColor];
    [self.view addSubview:bottomView];
    [self.view sendSubviewToBack:bottomView];
    
    
    [(UITextField*)[self.view viewWithTag:10000] setFont:[UIFont fontWithName:@"RobotoSlab-Bold" size:IS_IPAD?28:15]];
    [(UITextField*)[self.view viewWithTag:20000] setFont:[UIFont fontWithName:@"RobotoSlab-Bold" size:IS_IPAD?28:15]];
    [(UITextField*)[self.view viewWithTag:30000] setFont:[UIFont fontWithName:@"RobotoSlab-Bold" size:IS_IPAD?28:15]];
    [(UITextField*)[self.view viewWithTag:40000] setFont:[UIFont fontWithName:@"RobotoSlab-Bold" size:IS_IPAD?28:15]];
    if (puzzleArray.count)
    {
        [(UITextField*)[self.view viewWithTag:10000] setText:[[puzzleArray objectAtIndex:[currentPuzzleNumber intValue]-1] valueForKey:@"Option1"]];
        [(UITextField*)[self.view viewWithTag:20000] setText:[[puzzleArray objectAtIndex:[currentPuzzleNumber intValue]-1] valueForKey:@"Option2"]];
        [(UITextField*)[self.view viewWithTag:30000] setText:[[puzzleArray objectAtIndex:[currentPuzzleNumber intValue]-1] valueForKey:@"Option3"]];
        [(UITextField*)[self.view viewWithTag:40000] setText:[[puzzleArray objectAtIndex:[currentPuzzleNumber intValue]-1] valueForKey:@"Option4"]];
    }
    
    int x=0;
    int y=0;
    
    x+=(isIpad?133:21);
    y+=(isIpad?40:10);
    
    int index = 0 ;
    int appendindex = 0 ;
    if (![currentPuzzleName isEqualToString:@""]) {
        
        NSString *puzzleStr = [NSString stringWithFormat:@"%@",[currentPuzzleName stringByReplacingOccurrencesOfString:@" " withString:@""]];
        self.appendedPuzzleArray=[[NSMutableArray alloc]initWithCapacity:puzzleStr.length];
        float letterWidth = isIpad?61.0f:32.0f;
        float letterheight = isIpad?55.0f:32.0f;
        float gap = isIpad?16.0f:7.0f;
        
//        NSLog(@"self.appendedPuzzleArray==>%@",self.appendedPuzzleArray);
        
        for (int i=0; i<currentPuzzleName.length; i++) {
            
            NSString *c= [NSString stringWithFormat:@"%c",[currentPuzzleName characterAtIndex:i]];
            if (![c isEqualToString:@" "]) {
                [self.appendedPuzzleArray setObject:@"" atIndexedSubscript:appendindex];
                
                float cx = (bottomView.frame.size.width/2.0f) - ((letterWidth+gap)*(puzzleStr.length<7?puzzleStr.length:7)/2.0f) + (index * (letterheight+gap)) +(isIpad?gap:0);
//                NSLog(@"cx==>%f",cx);
                
                UIButton *puzzleAlphabetButton=[[UIButton alloc] initWithFrame:CGRectMake(cx,y, letterWidth, letterheight)];
                puzzleAlphabetButton.tag = 100+appendindex;
                [puzzleAlphabetButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [puzzleAlphabetButton setBackgroundImage:[UIImage imageNamed:[CommonFunctions getImageNameForName:@"blue_keyboard"]] forState:UIControlStateNormal];
                [puzzleAlphabetButton.titleLabel setFont:[UIFont fontWithName:@"RobotoSlab-Bold" size:IS_IPAD?35:20]];
                [puzzleAlphabetButton setTitle:@"" forState:UIControlStateNormal];
                [puzzleAlphabetButton addTarget:self action:@selector(revertAlphabetButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
                [bottomView addSubview:puzzleAlphabetButton];
                
                 appendindex++;

            }
            else
            {
                int cx = bottomView.frame.size.width/2.0f - (letterWidth+gap)*puzzleStr.length/2.0f+index * (letterheight+gap);
                
                UIButton *dashButton=[[UIButton alloc] initWithFrame:CGRectMake((cx+letterWidth),y, letterWidth, letterheight)];
                [dashButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [dashButton setTitle:@"" forState:UIControlStateNormal];
                [dashButton setUserInteractionEnabled:NO];
                [bottomView addSubview:dashButton];
            }
//            NSLog(@"self.appendedPuzzleArray==>%@",self.appendedPuzzleArray);
            
            x+=letterWidth+gap;
            index++;
            
            if (index>6 && appendindex<8)
            {
                _midConstraint.constant =isiPhone5?17:0;//For Iphone only
                
                x=(isIpad?133:21);
                y+=letterheight+gap;
                index=0;
            }
        }
    }
    //*********** End of Creation Blank button as per Puzzle Length **************//
    
    int totalLength = 14;

    currentPuzzleName = [NSMutableString stringWithFormat:@"%@",[currentPuzzleName stringByReplacingOccurrencesOfString:@" " withString:@""]];
    puzzleString = [self randomizeString:currentPuzzleName length:totalLength];
    NSLog(@"Random Puzzle Name ==>%@",puzzleString);
    
    availablePuzzleArray = [[NSMutableArray alloc] initWithCapacity:totalLength];
    availabilityOfLetters = [[NSMutableArray alloc] init];
    available_chosenMapping = [[NSMutableDictionary alloc] init];
    revealedLetters = [[NSMutableSet alloc] init];
    
    for (int i = 0; i < totalLength; i++) {
        availabilityOfLetters[i] = @(YES);
        [availablePuzzleArray addObject:[NSString stringWithFormat:@"%c", [puzzleString characterAtIndex:i]]];
    }
    
    float lettergap = isIpad?10.0f:5.0f;
    float letterBtnHeight = isIpad?68.0f:38.0f;
    
    //For bottom button strat x,y point
    x=(isIpad?115:11);
    if (currentPuzzleName.length>6) {
        bottomView.frame = CGRectMake(0, isIpad?600:isiPhone5?350:305 , isIpad?768:320, (isIpad?110:65)*(2+currentPuzzleName.length/6+1));
        y+=isIpad?(letterBtnHeight*2):(letterBtnHeight*(isiPhone5?2:1));
    }
    else
        y+=isIpad?(letterBtnHeight*2):(letterBtnHeight*(isiPhone5?2:1)+lettergap+lettergap);
    
    index = 0 ;
    for (int i=0; i<totalLength/6; i++) {
        for (int j=0; j<(totalLength/(totalLength/6)); j++) {
            
            UIButton *alphabetButton=[UIButton buttonWithType:UIButtonTypeRoundedRect];
            alphabetButton.tag = 1000+index;
            [alphabetButton setFrame:CGRectMake(x,y,letterBtnHeight,letterBtnHeight)];
            [alphabetButton setBackgroundImage:[UIImage imageNamed:[CommonFunctions getImageNameForName:@"white_keyboard_but"]] forState:UIControlStateNormal];
            [alphabetButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [alphabetButton.titleLabel setFont:[UIFont fontWithName:@"RobotoSlab-Bold" size:isIpad?44.0f:22.0f]];
            [alphabetButton setTitle:[NSString stringWithFormat:@"%@",[availablePuzzleArray objectAtIndex:index]] forState:UIControlStateNormal];
            [alphabetButton addTarget:self action:@selector(alphabetButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
            [bottomView addSubview:alphabetButton];
            
            x+=letterBtnHeight+lettergap;
            
            index++;
        }
        
        x=(isIpad?115:12);
        y+=letterBtnHeight+lettergap;
    }
}

-(float)topOffset
{
    float offset = 0;
    if ([self respondsToSelector:@selector(topLayoutGuide)]) {
        //      offset = self.topLayoutGuide.length;
        offset = 25;
        //offset = 0;
    }
    return offset;
}
/***** Check level has already skiped or not *****/
-(BOOL)skipLevel
{
    NSLog(@"currentPuzzleNumber==>%@ \n puzzleArray==>%@",currentPuzzleNumber,puzzleArray);
    
    query = [NSString stringWithFormat:@"UPDATE PuzzleTable SET SolveFlag='0' WHERE id=%@",currentPuzzleNumber];
    [dbRecord executeQuery:query];
    
    if (puzzleArray.count)
    {
        [puzzleArray removeAllObjects];
        puzzleArray = [NSMutableArray arrayWithArray:[dbRecord getPuzzleData:@"SELECT * FROM PuzzleTable"]];
    }
    else
        puzzleArray = [[NSMutableArray alloc]initWithArray:[dbRecord getPuzzleData:@"SELECT * FROM PuzzleTable"]];
    
    return puzzleArray.count?YES:NO;
}

/***** Update letters after delete add and continue button press *****/
-(void)updateLetters
{
    for (int i = 0; i < self.appendedPuzzleArray.count; i++) {
        UIButton *btn = (UIButton*) [self.view viewWithTag:i+100];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setTitle:[self.appendedPuzzleArray objectAtIndex:i] forState:UIControlStateNormal];
    }
    
//    appendedPuzzleString=[NSMutableString stringWithFormat:@"%@",[self.appendedPuzzleArray componentsJoinedByString: @""]];
    
    NSArray *available = [NSArray arrayWithArray:availabilityOfLetters];
    for (int i = 0; i < availabilityOfLetters.count; i++) {
        UIButton *btn = (UIButton*) [self.view viewWithTag:1000+i];
        [btn setHidden:!((NSNumber*)available[i]).boolValue];
    }
    for (NSNumber* num in revealedLetters) {
        UIButton *btn = (UIButton*) [self.view viewWithTag:100+num.intValue];
        [btn setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
    }
}

/***** show alphabet animation *****/
-(void)animateLetters
{
    //    for (int i = 0; i < 6; i++) {
    //        for (int j = 0; j < 2; j++) {
    //            UIButton *btn = (UIButton*)[self.view viewWithTag:1000 + i + 6*j];
    //            [btn setHidden:YES];
    //        }
    //    }
    //
    
    int index = 0 ;
    for (int i=0; i<availablePuzzleArray.count/6; i++) {
        for (int j=0; j<(availablePuzzleArray.count/(availablePuzzleArray.count/6)); j++) {
            UIButton *btn = (UIButton*)[bottomView viewWithTag:1000+index];
            if (YES) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.15*(j + 6*i) * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [btn setHidden:NO];
                });
            }
            
            [UIView animateWithDuration:0.2 delay:0.15*(j + 6*i) options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
                btn.frame = CGRectMake(btn.frame.origin.x-5.0f, btn.frame.origin.y-5.0f, btn.frame.size.width+10.0f, btn.frame.size.height+10.0f);
            } completion:^(BOOL finished) {
                btn.frame = CGRectMake(btn.frame.origin.x+5.0f, btn.frame.origin.y+5.0f, btn.frame.size.width-10.0f, btn.frame.size.height-10.0f);
            }];
            index++;
        }
    }
}

/***** Create alphabatic random string *****/
- (NSString *)randomizeString:(NSString *)str length:(NSUInteger)totalLength
{
    str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSMutableString *output = [NSMutableString string];
    
    NSUInteger len = str.length;
    
    NSString *letters = [NSString stringWithFormat:@"%@ABCDEFGHIJKLMNOPQRSTUVWXYZ",str];
    NSMutableString *randomString = [NSMutableString stringWithCapacity: totalLength];
    for (int i=0; i<(totalLength-len); i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
    }
    
    randomString = [NSMutableString stringWithFormat:@"%@%@",str,randomString];
    
    for (NSUInteger i = 0; i < totalLength; i++) {
        NSInteger index = arc4random_uniform((unsigned int)randomString.length);
        [output appendFormat:@"%C", [randomString characterAtIndex:index]];
        [randomString replaceCharactersInRange:NSMakeRange(index, 1) withString:@""];
    }
    
    if (output.length < totalLength) {
        [self randomizeString:str length:totalLength];
    }
    
    return output;
}
-(BOOL)wordIsFull
{
    return currentPuzzleName.length == available_chosenMapping.count;
}

/***** check puzzle and create congratulation view *****/
-(void)checkIsFillCorrect
{
    if ([self wordIsFull]) {
        if ([appendedPuzzleString isEqualToString:currentPuzzleName])
        {
            BOOL coinsWon = YES;
            
            if ([currentPuzzleNumber intValue]%25==0)
                bonus=YES;
            else
                bonus=NO;
            
            if (!bonus)
                [self winWithID:[currentPuzzleNumber intValue]];
            else
                [self winBonus];
            
            [self updateCoins];
            
            if (((NSNumber*)[DEFAULTS objectForKey:@"sounds"]).boolValue) {
                AudioServicesPlaySystemSound(successSound);
            }
            
            numAnsweredInRow++;
            
            winView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            winView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6f];
            
            UILabel *rightLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, isIpad?100:50, self.view.frame.size.width, isIpad?80.0f:40)];
            rightLabel.backgroundColor = [UIColor clearColor];
            rightLabel.textAlignment = NSTextAlignmentCenter;
            rightLabel.textColor = [UIColor whiteColor];
            rightLabel.text = rightString;
            rightLabel.font = [UIFont fontWithName:@"RobotoSlab-Regular" size:IS_IPAD?30:15];
            [winView addSubview:rightLabel];
            
            float letterWidth = isIpad?60:30.0f;
            float gap = isIpad?10:5.0f;
            
            currentPuzzleName = [[puzzleArray objectAtIndex:[currentPuzzleNumber intValue]-1] valueForKey:@"PuzzleName"];

            int y=isIpad?220:105;

            int index = 0 ;
            for (int i = 0; i < currentPuzzleName.length; i++)
            {
                float lx = self.view.frame.size.width/2.0f - (letterWidth+gap)*(currentPuzzleName.length<(isIpad?12:7)?currentPuzzleName.length:(isIpad?10:7))/2.0f + index * (letterWidth+gap);
                
                UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(lx, y, letterWidth, letterWidth)];
                NSString *charStr= [NSString stringWithFormat:@"%c",[currentPuzzleName characterAtIndex:i]];
                lbl.text = [charStr isEqualToString:@" "]?@"-":charStr;
                lbl.backgroundColor = [charStr isEqualToString:@" "]?[UIColor clearColor]:UIColorFromRedGreenBlue(0, 39, 118);

                lbl.font = [UIFont fontWithName:@"RobotoSlab-Bold" size:IS_IPAD?46:23];
                lbl.textColor = [UIColor whiteColor];
                lbl.textAlignment = NSTextAlignmentCenter;
                [winView addSubview:lbl];
                
                index++;
                if (index>(isIpad?9:6))
                {
                    y+=letterWidth+gap;
                    index=0;
                }
            }

            y+= isIpad?100.0f:50.0f;

            UILabel *congratsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, y , isIpad?768.0f:320.0f, isIpad?80:40.0f)];
            congratsLabel.backgroundColor = [UIColor clearColor];
            congratsLabel.textAlignment = NSTextAlignmentCenter;
            congratsLabel.textColor = [UIColor whiteColor];
            congratsLabel.font = [UIFont fontWithName:@"RobotoSlab-Bold" size:IS_IPAD?50:25];
            congratsLabel.text = Congrats;
            [winView addSubview:congratsLabel];
           
            y+=letterWidth+gap;

            UILabel *badgeLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2.0f - 150.0f, y, 300.0f, isIpad?80:40.0f)];
            badgeLabel.backgroundColor = [UIColor clearColor];
            badgeLabel.textAlignment = NSTextAlignmentCenter;
            badgeLabel.textColor = [UIColor whiteColor];
            badgeLabel.text = [NSString stringWithFormat:@"%d", [currentPuzzleNumber intValue]+1];
            badgeLabel.font = [UIFont fontWithName:@"RobotoSlab-Regular" size:IS_IPAD?48:24];
            [winView addSubview:badgeLabel];
            
            if (!bonus) {
                if (coinsWon) {
                    for (int i = 0; i < 4; i++) {
                        UIImageView *coinImg = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2.0f - ((float)i-1.5f)*30-10, self.view.frame.size.height+10, isIpad?35:26, isIpad?20:15)];
                        coinImg.image = [UIImage imageNamed:[CommonFunctions getImageNameForName:@"single_coin"]];
                        [winView addSubview:coinImg];
                        
                        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^(void) {
                            coinImg.frame = CGRectMake(self.view.frame.size.width/2.0f - ((float)i-1.5f)*30-10, y+(isIpad?130.0f:45.0f), isIpad?35:26, isIpad?20:15);
                        } completion:^(BOOL finished) {
                            [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^(void) {
                                coinImg.frame = CGRectMake(self.view.frame.size.width/2.0f - ((float)i-1.5f)*30-10, y+(isIpad?130.0f:45.0f), isIpad?35:26, isIpad?20:15);
                            } completion:^(BOOL finished) {
                                [UIView animateWithDuration:0.5 delay:i*0.1 options:UIViewAnimationOptionCurveEaseInOut animations:^(void) {
                                    coinImg.frame = CGRectMake(self.view.frame.size.width-25, 15, 5, 5);
                                } completion:^(BOOL finished) {
                                    [coinImg removeFromSuperview];
                                }];
                            }];
                        }];
                    }
                }
            } else {
                for (int j = 0; j < 4; j++) {
                    for (int i = 0; i < 5; i++) {
                        UIImageView *coinImg = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2.0f - ((float)i-2.0f)*30-10, self.view.frame.size.height+10, isIpad?35:26, isIpad?20:15)];
                        coinImg.image = [UIImage imageNamed:[CommonFunctions getImageNameForName:@"single_coin"]];
                        [winView addSubview:coinImg];
                        
                        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^(void) {
                            coinImg.frame = CGRectMake(self.view.frame.size.width/2.0f - ((float)i-2.0f)*30-10, y+(isIpad?160.0f:90.0f) + j*30-45.0f, isIpad?35:26, isIpad?20:15);
                        } completion:^(BOOL finished) {
                            [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^(void) {
                                coinImg.frame = CGRectMake(self.view.frame.size.width/2.0f - ((float)i-2.0f)*30-10, y+(isIpad?160.0f:90.0f) + j*30-45.0f, isIpad?35:26, isIpad?20:15);
                            } completion:^(BOOL finished) {
                                [UIView animateWithDuration:0.5 delay:(i+j)*0.1 options:UIViewAnimationOptionCurveEaseInOut animations:^(void) {
                                    coinImg.frame = CGRectMake(self.view.frame.size.width-25, 15, 5, 5);
                                } completion:^(BOOL finished) {
                                    [coinImg removeFromSuperview];
                                }];
                            }];
                        }];
                    }
                }
            }
            
            UIButton *continueBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            continueBtn.frame = CGRectMake(isIpad?185:50.0f, self.view.frame.size.height-(isIpad?320:isiPhone5?160:100),isIpad?398:221,isIpad?63:35);
            [continueBtn setTitle:NSLocalizedString(@"Continue", nil) forState:UIControlStateNormal];
            [continueBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [continueBtn setBackgroundImage:[UIImage imageNamed:[CommonFunctions getImageNameForName:@"green_but"]] forState:UIControlStateNormal];
            [continueBtn.titleLabel setFont:[UIFont fontWithName:@"RobotoSlab-Bold" size:IS_IPAD?44:22]];
            [continueBtn addTarget:self action:@selector(next) forControlEvents:UIControlEventTouchUpInside];
            [winView addSubview:continueBtn];
            
            [self.view.window addSubview:winView];
        }
        else {
            blinkAnimate = YES;
            for (int i = 0; i < currentPuzzleName.length; i++) {
                UIButton *btn = (UIButton*) [self.view viewWithTag:100+i];
                [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            }
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                if (blinkAnimate) {
                    for (int i = 0; i < currentPuzzleName.length; i++) {
                        UIButton *btn = (UIButton*) [self.view viewWithTag:100+i];
                        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    }
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        if (blinkAnimate) {
                            for (int i = 0; i < currentPuzzleName.length; i++) {
                                UIButton *btn = (UIButton*) [self.view viewWithTag:100+i];
                                [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                            }
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                if (blinkAnimate) {
                                    for (int i = 0; i < currentPuzzleName.length; i++) {
                                        UIButton *btn = (UIButton*) [self.view viewWithTag:100+i];
                                        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                                    }
                                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                        if (blinkAnimate) {
                                            for (int i = 0; i < currentPuzzleName.length; i++) {
                                                UIButton *btn = (UIButton*) [self.view viewWithTag:100+i];
                                                [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    }
}

-(void)dismissBonus
{
    [winView removeFromSuperview];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)next
{
    if (numAnsweredInRow == 5 && !_premium) {
        [[RevMobAds session] showFullscreen];
    }
    numAnsweredInRow = numAnsweredInRow % 5;
                                   
    [winView removeFromSuperview];
    [bottomView removeFromSuperview];
    
   
    if ([currentPuzzleNumber intValue] < puzzleArray.count)
    {
         currentPuzzleNumber = [NSString stringWithFormat:@"%d",([currentPuzzleNumber intValue]+1)];
/*        if (![[[puzzleArray objectAtIndex:[currentPuzzleNumber intValue]-1] objectForKey:@"SolveFlag"] intValue])
        {
            if (![dbRecord checkStatus:[[puzzleArray objectAtIndex:([currentPuzzleNumber intValue])] valueForKey:@"id"]])
            {
                currentPuzzleNumber = [NSString stringWithFormat:@"%d",([currentPuzzleNumber intValue]+1)];
                [self next];
            }
        }
commented for further*/
    }
    else
        currentPuzzleNumber = [NSString stringWithFormat:@"1"];
    
    [DEFAULTS setObject:currentPuzzleNumber forKey:@"currentPuzzleNo"];
    [DEFAULTS synchronize];
    
    _numAnsweredLabel.text = [NSString stringWithFormat:NSLocalizedString(@"StartPlay", nil), [currentPuzzleNumber intValue]];
    
    NSRange range = NSMakeRange(0,[appendedPuzzleString length]);
    [appendedPuzzleString replaceCharactersInRange:range withString:@""];
    
    [self changeView];
    
    [self performSelector:@selector(animateLetters) withObject:nil afterDelay:0.1f];
}
-(int)deleteIncorrectLetters
{
//    NSLog(@"availableLetters==>%@",self.appendedPuzzleArray);
    NSMutableString *w = [NSMutableString stringWithString:currentPuzzleName];
    for (int key=0;key<self.appendedPuzzleArray.count ; key++) {
        NSRange r = [w rangeOfString:[self.appendedPuzzleArray objectAtIndex:key]];
        if (r.location != NSNotFound) {
            [w deleteCharactersInRange:r];
        }
    }
//    NSLog(@"availableLetters==>%@",self.appendedPuzzleArray);
    NSMutableArray *availability = [NSMutableArray arrayWithArray:availabilityOfLetters];
    int numAvailable = 0;
    for (int i = 0; i < availability.count; i++) {
        if (((NSNumber*)availability[i]).boolValue) {
            numAvailable++;
            
            NSRange r = [w rangeOfString:availablePuzzleArray[i]];
            if (r.location != NSNotFound) {
                [w deleteCharactersInRange:r];
                availability[i] = [NSNumber numberWithBool:NO];
                numAvailable--;
            }
        }
    }

    int numDeleted = 0;
    int numToDelete = 24;
    
//NSLog(@"availabilityOfLetters==>%@",availabilityOfLetters);
    for (int i = 0; i < numToDelete && numAvailable > 0; i++) {
        int index = rand() % availablePuzzleArray.count;
        for (int j = 0; j < availablePuzzleArray.count; j++) {
            if (((NSNumber*)availability[(index+j)%availablePuzzleArray.count]).boolValue) {
                numAvailable--;
                availabilityOfLetters[(index+j)%availablePuzzleArray.count] = @(NO);
                availability[(index+j)%availablePuzzleArray.count] = @(NO);
                numDeleted++;
                break;
            }
        }
    }
    
    return numDeleted;
}

-(BOOL)showCorrectLetter
{
    if (currentPuzzleName.length != available_chosenMapping.count) {
        
        NSMutableString *w = [NSMutableString stringWithString:currentPuzzleName];
        for (NSNumber *key in available_chosenMapping) {
            NSRange r = [w rangeOfString:availablePuzzleArray[key.intValue]];
            if (r.location != NSNotFound) {
                [w deleteCharactersInRange:r];
            }
        }
        
        int index = rand() % availablePuzzleArray.count;
        for (int i = 0; i < availablePuzzleArray.count; i++) {
            if (((NSNumber*)availabilityOfLetters[(index+i)%availablePuzzleArray.count]).boolValue) {
                NSRange r = [w rangeOfString:availablePuzzleArray[(index+i)%availablePuzzleArray.count]];
                if (r.location != NSNotFound) {
                    
                    int avidx = (index+i)%availablePuzzleArray.count;
                    for (int j = 0; j < currentPuzzleName.length; j++) {
                        if ([self.appendedPuzzleArray[j] isEqualToString:@""] && [[NSString stringWithFormat:@"%c", [currentPuzzleName characterAtIndex:j]] isEqualToString:availablePuzzleArray[avidx]]) {
                            int wordidx = j;
                            
                            available_chosenMapping[[NSNumber numberWithInt:avidx]] = [NSNumber numberWithInt:wordidx];
                            self.appendedPuzzleArray[wordidx] = availablePuzzleArray[avidx];
                            availabilityOfLetters[avidx] = @(NO);
                            
                            appendedPuzzleString=[NSMutableString stringWithFormat:@"%@",[self.appendedPuzzleArray componentsJoinedByString: @""]];
                            
                            [revealedLetters addObject:@(wordidx)];
                            
                            return YES;
                        }
                    }
                }
            }
        }
    }
    return NO;
}

-(void)updateCoins
{
    [_coinButton setTitle:[NSString stringWithFormat:@"       %d", coins] forState:UIControlStateNormal];
}

-(void)deleteObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)payCoins:(int)coinsCount
{
    coins -= coinsCount;
    [DEFAULTS setObject:[NSNumber numberWithInt:coins] forKey:@"numCoins"];
    [DEFAULTS synchronize];
}

-(void)winWithID:(int)currentPuzzleIndex
{
    if ([[[puzzleArray objectAtIndex:[currentPuzzleNumber intValue]-1] objectForKey:@"SolveFlag"] intValue]) {
    }
    else
    {
        query = [NSString stringWithFormat:@"UPDATE PuzzleTable SET SolveFlag='1' WHERE id=%@",currentPuzzleNumber];
        [dbRecord executeQuery:query];
        
        if (puzzleArray.count)
        {
            [puzzleArray removeAllObjects];
            puzzleArray = [NSMutableArray arrayWithArray:[dbRecord getPuzzleData:@"SELECT * FROM PuzzleTable"]];
        }
        else
            puzzleArray = [[NSMutableArray alloc]initWithArray:[dbRecord getPuzzleData:@"SELECT * FROM PuzzleTable"]];
        
        coins += winCoins;
        [DEFAULTS setObject:[NSNumber numberWithInt:coins] forKey:@"numCoins"];
        [DEFAULTS synchronize];
    }
}
-(void)winBonus
{
    if ([[[puzzleArray objectAtIndex:[currentPuzzleNumber intValue]-1] objectForKey:@"SolveFlag"] intValue]) {
    }
    else
    {
        query = [NSString stringWithFormat:@"UPDATE PuzzleTable SET SolveFlag='1' WHERE id=%@",currentPuzzleNumber];
        [dbRecord executeQuery:query];
        
        if (puzzleArray.count)
        {
            [puzzleArray removeAllObjects];
            puzzleArray = [NSMutableArray arrayWithArray:[dbRecord getPuzzleData:@"SELECT * FROM PuzzleTable"]];
        }
        else
            puzzleArray = [[NSMutableArray alloc]initWithArray:[dbRecord getPuzzleData:@"SELECT * FROM PuzzleTable"]];
        
        coins += bonusCoins;
        [DEFAULTS setObject:[NSNumber numberWithInt:coins] forKey:@"numCoins"];
        [DEFAULTS synchronize];
    }
}

-(BOOL)premium
{
    return _premium;
};

#pragma mark-
#pragma mark Methods for animate buttons
/***** this is for start auto-refresh functionality******/
-(void) startTimer
{
    int refreshValue=5;
    zikzakTimer = [NSTimer scheduledTimerWithTimeInterval:refreshValue target:self selector:@selector(onTimer:) userInfo: nil repeats:YES];
}

-(void)onTimer:(NSTimer *)timer
{
    /******** get all new orders details *******/
    [NSThread detachNewThreadSelector:@selector(animateButtons) toTarget:self withObject:nil];
}
- (void)animateButtons
{
        [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
            _facebookButton.transform = CGAffineTransformRotate(CGAffineTransformIdentity,0);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
                _facebookButton.transform = CGAffineTransformRotate(CGAffineTransformIdentity,M_2_PI );
            } completion:^(BOOL finished) {
                 _facebookButton.transform = CGAffineTransformRotate(CGAffineTransformIdentity,0);
            }];
        }];
    
    //    [UIView beginAnimations:@"shrink" context:(__bridge void *)(button)];
    //    [UIView animateWithDuration:0.7f delay:0 options:UIViewAnimationOptionAutoreverse |                            UIViewAnimationCurveEaseInOut | UIViewAnimationOptionRepeat |     UIViewAnimationOptionAllowUserInteraction  animations:^{
    //
    //        [UIView setAnimationRepeatCount:3];
    //
    //        CGAffineTransform t  = CGAffineTransformMakeScale(1.2f, 1.2f);
    //
    //        for (UIButton button in self.destinationButtonsArray) {
    //            button.transform = t;
    //        }
    //    } completion:^(BOOL finished) {
    //        for (UIButton button in self.destinationButtonsArray) {
    //            button.transform = CGAffineTransformMakeScale(1.0f, 1.0f);}];
    //    }
    //     }];
    
    //    [UIView animateKeyframesWithDuration:0.1 delay:0.0 options:UIViewKeyframeAnimationOptionLayoutSubviews animations:^{
    //        [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:0.15 animations:^{
    //            //90 degrees (clockwise)
    //            _facebookButton.transform = CGAffineTransformMakeRotation(M_PI * -1.5);
    //        }];
    //        [UIView addKeyframeWithRelativeStartTime:0.15 relativeDuration:0.10 animations:^{
    //            //180 degrees
    //            _facebookButton.transform = CGAffineTransformMakeRotation(M_PI * 1.0);
    //        }];
    //        [UIView addKeyframeWithRelativeStartTime:0.25 relativeDuration:0.20 animations:^{
    //            //Swing past, ~225 degrees
    //            _facebookButton.transform = CGAffineTransformMakeRotation(M_PI * 1.3);
    //        }];
    //        [UIView addKeyframeWithRelativeStartTime:0.45 relativeDuration:0.20 animations:^{
    //            //Swing back, ~140 degrees
    //            _facebookButton.transform = CGAffineTransformMakeRotation(M_PI * 0.8);
    //        }];
    //        [UIView addKeyframeWithRelativeStartTime:0.65 relativeDuration:0.35 animations:^{
    //            //Spin and fall off the corner
    //            //Fade out the cover view since it is the last step
    //            CGAffineTransform shift = CGAffineTransformMakeTranslation(180.0, 0.0);
    //            CGAffineTransform rotate = CGAffineTransformMakeRotation(M_PI * 0.3);
    ////            _facebookButton.transform = CGAffineTransformConcat(shift, rotate);
    //            _facebookButton.transform =CGAffineTransformIdentity;
    //        }];
    //
    //    } completion:^(BOOL finished) {
    //        
    //    }];
}

#pragma mark-
#pragma mark UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 10001) {
        if (buttonIndex == 1) {
            //del 3. cost 80
            int num = [self deleteIncorrectLetters];
            if (num > 0) {
                [self payCoins:deleteCoins];
            }
            
            [self updateLetters];
        }
    }
    else if (alertView.tag == 10002) {
        if (buttonIndex == 1) {
            //show 1. cost 60
            BOOL replaced = [self showCorrectLetter];
            if (replaced) {
                [self payCoins:showCoins];
            }
            [self updateLetters];
            [self checkIsFillCorrect];
            
        }
    }
    else if (alertView.tag == 10003) {
        if (buttonIndex == 1) {
            //show 3. cost 100
            BOOL replaced = [self skipLevel];
            if (replaced) {
                [self payCoins:skipCoins];
            }
            [self next];
        }
    }
    else if (alertView.tag == 10004) {
        [self getCoins];
    }
    
    [self updateCoins];
}


@end
