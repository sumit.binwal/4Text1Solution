//
//  PlayVC.h
//  PlayGame
//
//  Created by Chandan Kumar on 07/03/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import <StoreKit/StoreKit.h>
#import <QuartzCore/QuartzCore.h>
#import <RevMobAds/RevMobAds.h>
#import <AudioToolbox/AudioToolbox.h>

@interface PlayVC : UIViewController<UIAlertViewDelegate>
{
    DatabaseHandler *dbRecord;
    
    UIView *bottomView;
    UIView *winView;
    
    NSString *query;
    
    NSMutableString *currentPuzzleName;
    NSMutableString *appendedPuzzleString;
    NSString *puzzleString;
        
    NSMutableArray *availablePuzzleArray;
    NSMutableDictionary *available_chosenMapping;
    NSMutableArray *availabilityOfLetters;
    
    NSMutableSet* revealedLetters;
    
    int numAnsweredInRow;
    
     NSTimer *zikzakTimer;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *midConstraint;
@property (weak, nonatomic) IBOutlet UIButton *facebookButton;

@property(nonatomic , strong) NSMutableString *currentPuzzleName;
@property(nonatomic , strong) NSMutableString *appendedPuzzleString;
@property(nonatomic , strong) NSMutableArray *appendedPuzzleArray;

@property (weak, nonatomic) IBOutlet UILabel *numAnsweredLabel;
@property (weak, nonatomic) IBOutlet UIButton *coinButton;

-(void)updateCoins;
@end
