//
//  HomeViewController.h
//  PlayGame
//
//  Created by Chandan Kumar on 04/03/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface HomeViewController : UIViewController<MFMailComposeViewControllerDelegate,NSXMLParserDelegate>
{
    DatabaseHandler *dbRecord;
    
    NSString *query;
    
    NSMutableArray *moreArray;
    
    BOOL _accumulatingParsedCharacterData;
    BOOL _didAbortParsing;
    int index;
    NSMutableDictionary *tempDic;
    
    NSTimer *refreshTimer;
}

#pragma mark -
#pragma mark -IBOutlets
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstMidConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *midConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;

@property (weak, nonatomic) IBOutlet UILabel *puzzleNumLevel;

@property (weak, nonatomic) IBOutlet UISwitch *switchButton;

@property (weak, nonatomic) IBOutlet UIScrollView *moreScrollview;

#pragma mark -
#pragma mark -IBAction
- (IBAction)playButtonPressedTouchUpInside:(id)sender;
- (IBAction)faceBookButtonPressed:(id)sender;
- (IBAction)twitterButtonPressed:(id)sender;
- (IBAction)mailButtonPressed:(id)sender;
- (IBAction)switchHideButtonPressed:(id)sender;

@end
